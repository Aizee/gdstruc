package com.gdstruc.module1;

public class PlayerNode {
    private Player player;
    private PlayerNode nextPlayerNode;
    private PlayerNode previousPlayerNode;

    public PlayerNode getPreviousPlayerNode() {
        return previousPlayerNode;
    }

    public void setPreviousPlayerNode(PlayerNode previousPlayerNode) {
        this.previousPlayerNode = previousPlayerNode;
    }

    public PlayerNode(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerNode getNextPlayerNode() {
        return nextPlayerNode;
    }

    public void setNextPlayer(PlayerNode nextPlayer) {
        this.nextPlayerNode = nextPlayer;
    }

    @Override
    public String toString() {
        return player.toString();
    }
}
