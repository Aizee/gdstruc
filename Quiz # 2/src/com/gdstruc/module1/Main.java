package com.gdstruc.module1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Player asuna = new Player(1, "Asuna", 100);
        Player lethalBacon = new Player(2, "LethalBacon", 205);
        Player hpDeskjet = new Player(3, "HPDeskjet", 34);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);

        playerLinkedList.printList();
        System.out.println(playerLinkedList.getSize());


        System.out.println(playerLinkedList.indexOf(new Player(3, "HPDeskjet", 34)));

        playerLinkedList.removeHead();

        System.out.println(playerLinkedList.getHead().getNextPlayerNode().getPreviousPlayerNode());
        System.out.println(playerLinkedList.getHead().getPreviousPlayerNode());
    }
}
