package com.gdstruc.module1;

public class PlayerLinkedList {
    private PlayerNode head;
    private int size;

    public int getSize() {
        return size;
    }

    public PlayerNode getHead() {
        return head;
    }

    public void setHead(PlayerNode head) {
        this.head = head;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
        if (head.getNextPlayerNode() != null) {
            playerNode.getNextPlayerNode().setPreviousPlayerNode(head);
        }
        size++;
    }

    public void removeHead()
    {
        if (head != null) {
            head = head.getNextPlayerNode();
            head.setPreviousPlayerNode(null);
            size--;
        }
    }

    public boolean contains(Player player)
    {
        return indexOf(player) >= 0;
    }

    public int indexOf(Player player)
    {
        PlayerNode iterator = head;
        int index = -1;

        while (iterator != null)
        {
            index++;
            if (iterator.getPlayer().equals(player)) return index;
            iterator = iterator.getNextPlayerNode();
        }
        return -1;
    }

    public void printList() {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayerNode();
        }
        System.out.println("null");
    }
}
