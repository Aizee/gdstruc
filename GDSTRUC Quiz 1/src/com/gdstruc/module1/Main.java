package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        int[] numbers1 = new int[10];

        numbers1[0] = 35;
        numbers1[1] = 21;
        numbers1[2] = -23;
        numbers1[3] = 5;
        numbers1[4] = 101;
        numbers1[5] = 12;
        numbers1[6] = -1;
        numbers1[7] = 94;
        numbers1[8] = 321;
        numbers1[9] = 16;

        int[] numbers2 = new int[5];
        numbers2[0] = 0;
        numbers2[1] = 9;
        numbers2[2] = -20;
        numbers2[3] = 50;
        numbers2[4] = 89;

        System.out.println("Before Bubble Sort:");
        printArrayElements(numbers1);

        bubbleSort(numbers1);

        System.out.println("\nAfter Bubble Sort:");
        printArrayElements(numbers1);

        System.out.println("\nBefore Selection Sort:");
        printArrayElements(numbers2);

        selectionSort(numbers2);

        System.out.println("\nAfter Selection Sort:");
        printArrayElements(numbers2);
    }

    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i + 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 0; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr)
    {
        for (int j : arr)
        {
            System.out.print(j + " ");
        }
    }
}
