package com.gdstruc.quiz3;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

	    ArrayQueue matchmaking = new ArrayQueue(7);
	    int matchesFound = 0;

	    // array of names
		String[] playerNames = {"Deiru", "Uried", "Rudei", "Dirue", "Iredu", "Eridu", "Reidu", "Durei", "Riued"};

		// Loops for 10 games
		while (matchesFound < 10) {
			// queues up to 7 players
			for (int playersQueueing = ThreadLocalRandom.current().nextInt(1, 8); playersQueueing > 0; playersQueueing--) {
				// random variables
				int randomId = ThreadLocalRandom.current().nextInt(1, 1001);
				int randomName = ThreadLocalRandom.current().nextInt(1, playerNames.length);
				int randomLevel = ThreadLocalRandom.current().nextInt(1, 51);

				// adds new player to queue
				matchmaking.add(new Player(randomId, playerNames[randomName] + randomId, randomLevel));
				System.out.println("Queueing " + playerNames[randomName] + randomId);
			}

			scanner.nextLine();

			// match pops when there are 5 or more players in queue
			if (matchmaking.size() >= 5) {
				System.out.println("MATCH FOUND!\n");
				matchesFound++;

				// remove first 5 in queue
				for (int i = 0; i < 5; i++) {
					matchmaking.remove();
				}

				// print remaining in queue
				System.out.println("Remaining in queue:");
				if (matchmaking.size() == 0) System.out.println("None");
				matchmaking.printQueue();

				scanner.nextLine();
			}
		}
    }
}
