package com.gdstruc.midterms;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.ThreadLocalRandom;

public class CardStack {
    private LinkedList<Card> stack;

    public CardStack()
    {
        stack = new LinkedList<Card>();
    }

    public void push(Card card)
    {
        stack.push(card);
    }

    public Card pop()
    {
        return stack.pop();
    }

    public int size()
    {
        return stack.size();
    }

    public void transferCards(CardStack targetStack)
    {
        if (!this.isEmpty()) {
            for (int amount = ThreadLocalRandom.current().nextInt(1, 6); amount > 0; amount--) {
                if (this.isEmpty()) return;
                targetStack.push(this.pop());
            }
        } else {
            System.out.println("No cards found");
        }
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();

        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
}
