package com.gdstruc.midterms;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // array of names
        String[] cardNames = {"Blue Eyes White Dragon", "Musashi", "Decimate", "what am i holding", "Discard this", "im out of ideas"};

        // Generates stacks for hand, deck, and discard
        CardStack hand = new CardStack();
        CardStack deck = new CardStack();
        CardStack discardPile = new CardStack();

        // Populates deck
        for (int i = 0; i < 30; i++) {
            int randomCardName = ThreadLocalRandom.current().nextInt(0, cardNames.length);
            deck.push(new Card(cardNames[randomCardName]));
        }

        // Print deck
        System.out.println("Deck:");
        deck.printStack();

        // Loop until deck is empty
        while (!deck.isEmpty()) {
            // Executes random command
            int randomCommand = ThreadLocalRandom.current().nextInt(0, 3);
            if (randomCommand == 0) // draw from deck
            {
                System.out.println("\nDrawing cards...");
                deck.transferCards(hand);
            } else if (randomCommand == 1) // discard cards
            {
                System.out.println("\nDiscarding cards...");
                hand.transferCards(discardPile);
            } else // get from discard pile
            {
                System.out.println("\nGetting from discard pile...");
                discardPile.transferCards(hand);
            }

            scanner.nextLine();

            // Prints hand, deck size, discard pile size
            System.out.println("\nHand:");
            hand.printStack();
            System.out.println("\nRemaining cards in Deck: " + deck.size());
            System.out.println("Number of cards in Discard pile: " + discardPile.size());

            scanner.nextLine();
        }
    }
}
